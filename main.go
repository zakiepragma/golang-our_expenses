package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type Pengeluaran struct {
	ID         string `json:"id"`
	Title      string `json:"title"`
	Tanggal    string `json:"tanggal"`
	Jenis      string `json:"jenis"`
	Keterangan string `json:"keterangan"`
	AtasNama   string `json:"atasNama"`
}

var expanses []Pengeluaran

func getExpanses(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(expanses)
}

func deleteExpanse(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	for index, item := range expanses {
		if item.ID == params["id"] {
			expanses = append(expanses[:index], expanses[index+1:]...)
			break
		}
	}
	json.NewEncoder(w).Encode(expanses)
}

func getExpanse(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	for _, item := range expanses {
		if item.ID == params["id"] {
			json.NewEncoder(w).Encode(item)
			return
		}
	}
}

func createExpanse(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var expanse Pengeluaran
	_ = json.NewDecoder(r.Body).Decode(&expanse)
	expanse.ID = strconv.Itoa(rand.Intn(1000000))
	expanses = append(expanses, expanse)
	json.NewEncoder(w).Encode(expanse)
}

func updateExpanse(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	for index, item := range expanses {
		if item.ID == params["id"] {
			expanses = append(expanses[:index], expanses[index+1:]...)
			var expanse Pengeluaran
			_ = json.NewDecoder(r.Body).Decode(&expanses)
			expanse.ID = params["id"]
			expanses = append(expanses, expanse)
			json.NewEncoder(w).Encode(expanse)
			return
		}
	}
}

func main() {
	r := mux.NewRouter()

	expanses = append(expanses, Pengeluaran{
		ID:         "1",
		Title:      "belanja di warung",
		Tanggal:    "2023-07-19",
		Jenis:      "bahan makanan",
		Keterangan: "kebutuhan keluarga",
		AtasNama:   "istri",
	})

	r.HandleFunc("/expanses", getExpanses).Methods("GET")
	r.HandleFunc("/expanses/{id}", getExpanse).Methods("GET")
	r.HandleFunc("/expanses", createExpanse).Methods("POST")
	r.HandleFunc("/expanses/{id}", updateExpanse).Methods("PUT")
	r.HandleFunc("/expanses/{id}", deleteExpanse).Methods("DELETE")

	fmt.Println("start server at port 8080")
	log.Fatal(http.ListenAndServe(":8080", r))
}
